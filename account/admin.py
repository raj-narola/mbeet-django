from django.contrib import admin
from account.models import UserExtend, Role

# Register your models here.
admin.site.register(UserExtend)
admin.site.register(Role)