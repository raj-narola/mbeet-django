from django.conf.urls import url
from .views import getShowUser, Register, activate, Getuser, Adduser, deleteUser, editUser , getUserUnits


urlpatterns = [
    url(r'^getShowUser/$',getShowUser),
    url(r'^getUserUnits/$',getUserUnits),
    url(r'^addUser/$', Adduser),
    url(r'^editUser/$',editUser),
    url(r'^deleteUser/$', deleteUser),
    url(r'^registerUser/$', Register),
    url(r'^getUser/$', Getuser),    
    url(r'^activate/(?P<uidb64>[0-9A-Za-z_\-]+)/(?P<token>[0-9A-Za-z]{1,13}-[0-9A-Za-z]{1,20})/$',
        activate, name='activate'),
]
