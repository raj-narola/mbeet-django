from django.db import models
from django.contrib.auth.models import User

# Create your models here.

class Role(models.Model):
    name = models.CharField(max_length=128, null=True, blank=True)

    def __str__(self):
        return self.name


class UserExtend(models.Model):
    user = models.ForeignKey(User, null=True, blank=True)
    date_of_birth = models.DateField(blank=True, null=True)
    facebook_uid = models.CharField(max_length=128, blank=True)
    gmail_uid = models.CharField(max_length=128, blank=True)
    gender = models.PositiveIntegerField(blank=True, null=True)
    is_owner = models.BooleanField(default=False)
    otp = models.PositiveIntegerField(blank=True, null=True)
    phone = models.PositiveIntegerField(blank=True, null=True)
    phone_verified = models.BooleanField(default=False)
    twitter_uid = models.CharField(max_length=128, blank=True)
    verify_phone_sent_at = models.DateTimeField(blank=True, null=True)
    role = models.ForeignKey(Role, blank=True, null=True)

    def __str__(self):
        return self.user.first_name 


