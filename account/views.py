from django.shortcuts import render
from django.http import JsonResponse, HttpResponseRedirect
from django.contrib.auth import login as auth_login
import json
from django.contrib.auth.models import User
from django.contrib.auth import authenticate, logout
from django.contrib.auth.hashers import make_password
from rest_framework.decorators import api_view
from .models import UserExtend
from django.contrib.auth.models import User
from django.contrib.auth.hashers import make_password
from django.http import HttpResponse
from datetime import datetime
from unit.models import Unit
#For Email
from django.contrib.sites.shortcuts import get_current_site
from django.utils.encoding import force_bytes, force_text
from django.utils.http import urlsafe_base64_encode, urlsafe_base64_decode
from django.template.loader import render_to_string
from .tokens import account_activation_token
from django.core.mail import EmailMessage


def getShowUser(request):
    if request.method == "POST":
        print(request)
        body_unicode = request.body.decode('utf-8')
        body = json.loads(body_unicode)
        print("----------Register--------------")
        print(body)
        userid=body['userid']
        a=User.objects.get(id=userid)
        b=UserExtend.objects.get(user_id=userid)
        print(a)
        d1=[]
        # k=a.date_joined.date()
        k=a.date_joined
        print((b.date_of_birth))
        d1.append({"id":a.id,"first_name":a.first_name,"last_name":a.last_name,"gender":b.gender,"email":a.email,"email_verified":a.is_active,"phone_verified":b.phone_verified,"date_of_birth":b.date_of_birth,"joined":k,"phone":b.phone,"userid":a.id})
        di={}
        di["users"]=d1
        print("-------------------------------")
        return JsonResponse(di, status=200)


def Adduser(request):
    if request.method == "POST":
        print(request.body)
        #b=request.body.decode('utf-8')
        b = json.loads(request.body)
        v=b['body']
        print(v)
        first_name=v['firstname']
        last_name=v['lastname']
        email=v['email']
        phone=v['phonenumber']
        gender=v['gender']
        date_of_birth=v['date_of_birth']
        if(v['email_verified']):
            email_verified=1
        else:
            email_verified=0    
        if(v['phone_verified']):
            phone_verified=1
        else:
            phone_verified=0

        flag = 0
        r1 = User.objects.all()

        for i in r1:
            if i.username == email:
                flag = 1        

        if flag == 1:
            return JsonResponse({"error":"This email address is already registered" }, status=400)
        else:
            User.objects.create(username=email, email=email, first_name=first_name, last_name=last_name, password="password", is_active=False)
            b=User.objects.get(email=email)
            UserExtend.objects.create(user=b, phone=phone, gender=gender, date_of_birth=date_of_birth)
    return JsonResponse({"success": "Registeration successful,"}, status=200)
    


@api_view(['POST'])
def Register(request):
    if request.method == "POST":
        print(request)
        body_unicode = request.body.decode('utf-8')
        body = json.loads(body_unicode)
        print("----------Register--------------")
        body = body["values"]
        print(body)
        print("-------------------------------")
        first_name = body["firstname"]
        last_name = body["lastname"]
        email = body["email"]
        password = make_password(body["password"])
        gender = body["gender"]
        phonenumber = body["phonenumber"]
        date_of_birth = body["date_of_birth"]

        flag = 0
        r1 = User.objects.all()

        for i in r1:
            if i.username == email:
                flag = 1    
 
        if flag == 0:   
            User.objects.create(username=email, email=email, first_name=first_name, last_name=last_name, password=password, is_active=False)
            a = User.objects.get(email=email)

            current_site = get_current_site(request)
            print(current_site)
            print(type(current_site))


            mail_subject = 'Activate your mbeet account.'
            to_email = email
            message = "http://"+str(current_site)+"/accounts/activate/"+str(urlsafe_base64_encode(force_bytes(a.pk)).decode('utf-8'))+"/"+str(account_activation_token.make_token(a))+"/"
 
            email = EmailMessage(mail_subject, message, to=[to_email])
            email.send()
            
            UserExtend.objects.create(user=a, phone=phonenumber, gender=gender, date_of_birth=date_of_birth )


        else:
            return JsonResponse({"error":"This email address is already registered" }, status=400)

        return JsonResponse({"success": "Registeration successful, Please verify your email addresss."}, status=200)
    return JsonResponse({"error":"Fail to register" }, status=400)

@api_view(['POST'])
def login(request):
    if request.method == "POST":
        if request.user.is_authenticated():
            a = User.objects.get(id=request.user.id)
            cd = {
                "username":a.username,
                "is_superuser":a.is_superuser,
                "is_staff":a.is_staff,
                "email":a.email
            }
            #print(cd)
            return JsonResponse(cd, status=200)
        else:
            #print(request.body)
            body_unicode = request.body.decode('utf-8')
            body = json.loads(body_unicode)
            print("-----------Login--------------")
            print(body)
            body = body["values"]
            print("-------------------------------")
            username = body['email']
            password = body['password']
            user = authenticate(username=username, password=password)
            if user:
                auth_login(request, user)
                if request.user.is_authenticated:
                    #print(request.user.email)
                    a = User.objects.get(id=request.user.id)
                    cd = {
                        "username":a.username,
                        "is_superuser":a.is_superuser,
                        "is_staff":a.is_staff,
                        "email":a.email
                    }
                    print(cd)
                    return JsonResponse(cd, status=200)
            else:
                flag = 0
                a = User.objects.all()
                for i in a:
                    if(i.username == username):
                        if(i.is_active == False):
                            flag = 1

                if flag == 1:
                    return JsonResponse({'error':'Please verify your email addreess'}, status=400)
                else:
                    return JsonResponse({'error':'Please check username or password'}, status=400)
    return JsonResponse({'status':'false'}, status=404)



def activate(request, uidb64, token):
    print("Enter for activations=============>")
    try:
        uid = force_text(urlsafe_base64_decode(uidb64))
        user = User.objects.get(pk=uid)
    except(TypeError, ValueError, OverflowError, User.DoesNotExist):
        user = None
    if user is not None and account_activation_token.check_token(user, token):
        user.is_active = True
        user.save()
        login(request, user)
        return HttpResponseRedirect("/")
    else:
        return HttpResponse('Activation link is invalid!')



def Getuser(request):
    print("in get user")
    if request.user.is_authenticated():
        print("user:", request.user.id)
    a=User.objects.all()
    d1=[]
    di={}
    #print(a)
    k=None
    phone_verified=None
    for i in a:
        print("===========")
        b=UserExtend.objects.filter(user_id=i.id)
        for j in b:
            k=j.phone
            phone_verified=j.phone_verified
        print(i.id)
        print(i.first_name)
        print(i.email)
        print(i.is_active)
        d1.append({"id":i.id,"name":i.first_name,"email":i.email,"email_verified":i.is_active,"phone":k,"phone_verified":phone_verified})
        for i in d1:
            di["users"]=d1
        print(di)        
    return JsonResponse(di, status=200)



def deleteUser(request):
    if request.method == "POST":
        print(request)
        body_unicode = request.body.decode('utf-8')
        body = json.loads(body_unicode)
        print("----------Register--------------")
        print(body)
        userid=body['userid']
        User.objects.get(id=userid).delete()
        print("-------------------------------")
        return JsonResponse({"success": "User deleted"}, status=200)



def editUser(request):
    if request.method=='POST':
        print(request)
        body_unicode = request.body.decode('utf-8')
        body = json.loads(body_unicode)
        print("----------Edit user--------------")
        print(body)
        userid=body['userid']
        data=body['body']
        print(userid)
        print(data)
        try:
            a=User.objects.get(id=userid)
            b=UserExtend.objects.get(user_id=userid)
            if (a and b ):
                a.first_name=data['first_name']
                a.last_name=data['last_name']
                a.email=data['email']
                a.is_active=(data['email_verified'].capitalize())
                a.save()
                b.phone=data['phone']
                b.date_of_birth=data['date_of_birth']
                b.gender=data['gender']
                b.phone_verified=(data['phone_verified'].capitalize())
                b.save()
                return JsonResponse({"success": "User Edited"}, status=200)       
            else:
                return JsonResponse({'error':'User Edit Failed'}, status=400)
        except:
            return JsonResponse({'error':'User Edit Failed'}, status=400)
        return JsonResponse({'error':'User Edit Failed'}, status=400)
    return JsonResponse({'error':'User Edit Failed'}, status=400)    


def getUserUnits(request):
    if request.method == "POST":
        print(request)
        body_unicode = request.body.decode('utf-8')
        body = json.loads(body_unicode)
        print("----------getUserUnits--------------")
        print(body)
        userid=body['userid']
        print(userid)
        try:
            v=User.objects.get(id=userid)
            b=Unit.objects.filter(user_id=userid)
            d1=[]
            di={}
            for a in b:
                d1.append({"owner_name":v.first_name,"longitude":a.longitude,"latitude":a.latitude,"available_as":a.available_as,"id":a.id,"unit_class":a.unit_class,"unit_status":a.unit_enabled,"number_of_subunits":a.number_of_subunits,"title_en":a.title_em,"body_en":a.body_en,"address":a.address,"price":a.price,"number_of_guests":a.number_of_guests,"number_of_rooms":a.number_of_rooms,"number_of_baths":a.number_of_baths,"number_of_beds":a.number_of_beds,"created_at":a.created_at,"unit_type":a.unit_type,"total_area":a.total_area})
                di['units']=d1
            return JsonResponse(di, status=200)
        except:
            return JsonResponse({'error':'getUserUnits Failed'}, status=400)                 
    return JsonResponse({'error':'getUserUnits Failed'}, status=400)