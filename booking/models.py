from django.db import models
from django.contrib.auth.models import User
from unit.models import Unit
# Create your models here.
class Booking(models.Model):
    user_id= models.ForeignKey(User, null=True, blank=True)
    created_at = models.DateTimeField(auto_now_add=True)
    check_in = models.DateTimeField(auto_now_add=True)
    check_out = models.DateTimeField(auto_now_add=True)
    payment_status = models.PositiveIntegerField(blank=True, null=True)
    total_amount=models.FloatField(blank=True, null=True)
    booking_cancelled=models.BooleanField(default=False)
    unit_id=models.ForeignKey(Unit,null=True, blank=True)
    transaction_id=models.CharField(max_length=128, blank=True)

    def __str__(self):
        return str(self.id)
    
    
   