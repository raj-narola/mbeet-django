from django.conf.urls import url
from .views import addUnit, getUnits, deleteUnit, getUnit, updateUnit

urlpatterns = [
    url(r'^addUnit/$', addUnit),
    url(r'^getUnits/$',getUnits),
    url(r'^deleteUnit/$',deleteUnit),
    url(r'^getUnit/$',getUnit),
    url(r'^updateUnit/$',updateUnit)
]
