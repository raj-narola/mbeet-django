# -*- coding: utf-8 -*-
# Generated by Django 1.11.4 on 2018-04-20 07:35
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Unit',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('address', models.CharField(blank=True, max_length=128)),
                ('available_as', models.PositiveIntegerField(blank=True, null=True)),
                ('bed_type', models.PositiveIntegerField(blank=True, null=True)),
                ('latitude', models.FloatField(blank=True, null=True)),
                ('longitude', models.FloatField(blank=True, null=True)),
                ('mbeet_percentage', models.IntegerField(blank=True, null=True)),
                ('message', models.CharField(blank=True, max_length=128)),
                ('number_of_baths', models.PositiveIntegerField(blank=True, null=True)),
                ('number_of_beds', models.PositiveIntegerField(blank=True, null=True)),
                ('number_of_guests', models.PositiveIntegerField(blank=True, null=True)),
                ('number_of_rooms', models.PositiveIntegerField(blank=True, null=True)),
                ('number_of_subunits', models.PositiveIntegerField(blank=True, null=True)),
                ('price', models.FloatField(blank=True, null=True)),
                ('room_type', models.PositiveIntegerField(blank=True, null=True)),
                ('service_charge', models.FloatField(blank=True, null=True)),
                ('soft_delete', models.BooleanField(default=False)),
                ('total_area', models.FloatField(blank=True, null=True)),
                ('unit_class', models.CharField(blank=True, max_length=128)),
                ('unit_enabled', models.BooleanField(default=False)),
                ('unit_status', models.BooleanField(default=False)),
                ('unit_type', models.PositiveIntegerField(blank=True, null=True)),
            ],
        ),
    ]
