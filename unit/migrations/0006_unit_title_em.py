# -*- coding: utf-8 -*-
# Generated by Django 1.11.4 on 2018-05-03 04:40
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('unit', '0005_unit_body_en'),
    ]

    operations = [
        migrations.AddField(
            model_name='unit',
            name='title_em',
            field=models.CharField(blank=True, max_length=128),
        ),
    ]
