from django.contrib import admin
from unit.models import Rule, Unit, Amenity


# Register your models here.
admin.site.register(Rule)
admin.site.register(Unit)
admin.site.register(Amenity)