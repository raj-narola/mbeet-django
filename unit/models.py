from django.db import models
from django.contrib.postgres.fields.jsonb import JSONField


# Create your models here.
class Unit(models.Model):
    user_id= models.PositiveIntegerField(blank=True, null=True)
    address = models.CharField(max_length=128, blank=True)
    body_en=models.CharField(max_length=128, blank=True)
    available_as = models.PositiveIntegerField(blank=True, null=True)
    bed_type = models.PositiveIntegerField(blank=True, null=True)
    latitude = models.FloatField(blank=True, null=True)
    longitude = models.FloatField(blank=True, null=True)
    mbeet_percentage = models.IntegerField(blank=True, null=True)
    message = models.CharField(max_length=128, blank=True)
    number_of_baths = models.PositiveIntegerField(blank=True, null=True)
    number_of_beds = models.PositiveIntegerField(blank=True, null=True)
    number_of_guests = models.PositiveIntegerField(blank=True, null=True)
    number_of_rooms = models.PositiveIntegerField(blank=True, null=True)
    number_of_subunits = models.PositiveIntegerField(blank=True, null=True)
    price = models.FloatField(blank=True, null=True)
    room_type = models.PositiveIntegerField(blank=True, null=True)
    service_charge = models.FloatField(blank=True, null=True)
    soft_delete = models.BooleanField(default=False)
    total_area =  models.FloatField(blank=True, null=True)
    unit_class = models.CharField(max_length=128, blank=True)
    unit_enabled = models.BooleanField(default=False)
    unit_status = models.BooleanField(default=False)
    unit_type = models.PositiveIntegerField(blank=True, null=True)
    title_translations = JSONField(blank=True, null=True)
    body_translations = JSONField(blank=True, null=True)
    title_em=models.CharField(max_length=128, blank=True)
    created_at = models.DateTimeField(auto_now_add=True)

    def __str__(self):
        return self.title_em


class Rule(models.Model):
    status = models.BooleanField(default=False)
    name_translations = JSONField(blank=True, null=True)

class Amenity(models.Model):
    is_active = models.BooleanField(default=False)
    name_translations = JSONField(blank=True, null=True)