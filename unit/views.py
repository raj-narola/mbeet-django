from django.shortcuts import render
from django.http import JsonResponse, HttpResponseRedirect
from django.contrib.auth import login as auth_login
import json
from django.contrib.auth.models import User
from django.contrib.auth import authenticate, logout
from django.contrib.auth.hashers import make_password
from rest_framework.decorators import api_view
from django.contrib.auth.models import User
from django.contrib.auth.hashers import make_password
from django.http import HttpResponse
from datetime import datetime
from .models import Unit
#For Email
from django.contrib.sites.shortcuts import get_current_site
from django.utils.encoding import force_bytes, force_text
from django.utils.http import urlsafe_base64_encode, urlsafe_base64_decode
from django.template.loader import render_to_string
from django.core.mail import EmailMessage

# Create your views here.

def getUnits(request):
    print("in get Units")
    if request.user.is_authenticated():
        print("user:", request.user.id)
    a=Unit.objects.all()
    d1=[]
    di={}
    #print(a)
    k=None
    for i in a:
        print("===========")
        b=User.objects.get(id=i.user_id)
        d1.append({"id":i.id,"unit_status":i.unit_enabled,"number_of_subunits":i.number_of_subunits,"owner_name":b.first_name,"title_en":i.body_en,"address":i.address,"price":i.price,"number_of_guests":i.number_of_guests,"number_of_rooms":i.number_of_rooms,"number_of_baths":i.number_of_baths,"number_of_beds":i.number_of_beds})
        for i in d1:
            di["units"]=d1
        print(di)        
    return JsonResponse(di, status=200)



def addUnit(request):
    if request.method == "POST":
        print(request.body)
        print(request.user.id)
        body_unicode = request.body.decode('utf-8')
        body = json.loads(body_unicode)
        print("----------Unit add--------------")
        body = body["body"]
        print(body)
        print("-------------------------------")
        title_en=body["title_en"]
        body_en= body["body_en"]
        unit_enabled= body["unit_enabled"]
        if(unit_enabled=='false'):
            unit_enabled=False
        else:
            unit_enabled=True    
        price=body["price"]
        unit_type=body["unit_type"]
        unit_class=body["unit_class"]
        total_area=body["total_area"]
        sub_unit_fake=body["sub_unit_fake"]
        latitude=body["latitude"]
        longitude=body["longitude"]
        available_as=body["available_as"]
        number_of_guests=body["number_of_guests"]
        number_of_rooms=body["number_of_rooms"]
        number_of_beds=body["number_of_beds"]
        number_of_baths=body["number_of_baths"]
        bed_type=body["bed_type"]
        user_id=request.user.id
        Unit.objects.create(user_id=user_id,title_em=title_en,body_en=body_en,available_as=available_as,bed_type=bed_type,latitude=latitude,longitude=longitude,number_of_baths=number_of_baths,number_of_beds=number_of_beds,number_of_guests=number_of_guests,number_of_rooms=number_of_rooms,number_of_subunits=sub_unit_fake,price=price,room_type=bed_type,total_area=total_area,unit_class=unit_class,unit_enabled=unit_enabled,unit_type=unit_type,)

        return JsonResponse({"success": "Registeration successful,"}, status=200)
    return JsonResponse({"error":"This email address is already registered" }, status=400)


def deleteUnit(request):
    if request.method == "POST":
        print(request.body)
        print(request.user.id)
        body_unicode = request.body.decode('utf-8')
        body = json.loads(body_unicode)
        print("----------Unit delete--------------")
        body = body["unitid"]
        print(body)
        print("-------------------------------")
        try:
            Unit.objects.get(id=body).delete()
            return JsonResponse({"success": "Unit deleted successfully"}, status=200)
        except:
            return JsonResponse({"error":"Error in Unit delete" }, status=400)
        return JsonResponse({"error":"Error in Unit delete" }, status=400)
    return JsonResponse({"error":"Error in Unit delete" }, status=400)    
       

def getUnit(request):
    print("hiiii")
    print(request.body)
    print(request.user.id)
    body_unicode = request.body.decode('utf-8')
    body = json.loads(body_unicode)
    print("----------Unit get--------------")
    body = body["unitid"]
    print(body)
    print("-------------------------------")
    d1=[]
    di={}
    try:
        print(body)
        a=Unit.objects.get(id=body)
        print(a)
        print("==================")   
        b=User.objects.get(id=a.user_id)
        d1.append({"longitude":a.longitude,"latitude":a.latitude,"available_as":a.available_as,"id":a.id,"unit_class":a.unit_class,"unit_status":a.unit_enabled,"number_of_subunits":a.number_of_subunits,"owner_name":b.first_name,"title_en":a.title_em,"body_en":a.body_en,"address":a.address,"price":a.price,"number_of_guests":a.number_of_guests,"number_of_rooms":a.number_of_rooms,"number_of_baths":a.number_of_baths,"number_of_beds":a.number_of_beds,"created_at":a.created_at,"unit_type":a.unit_type,"total_area":a.total_area})
        di["unit"]=d1
        print(di)
        return JsonResponse(di, status=200)
    except:
        print("in except")
        return JsonResponse({"error":"Error in Unit delete" }, status=400)    
    return JsonResponse({"error":"Error in Unit delete" }, status=400)        

def updateUnit(request):
    if request.method == "POST":
        print("hiiii")
        print(request.body)
        print(request.user.id)
        body_unicode = request.body.decode('utf-8')
        body = json.loads(body_unicode)
        print("----------Unit update--------------")
        unitid = body["unitid"]
        values = body["values"]
        print(unitid)
        print(values)
        print("-------------------------------")
        try:
            print("inside try")
            a=Unit.objects.get(id=unitid)
            print("a found")
            a.title_em=values['title_en']
            a.body_en=values['body_en']
            a.price=values['price']
            a.unit_type=values['unit_type']
            a.unit_class=values['unit_class']
            a.total_area=values['total_area']
            a.number_of_subunits=values['sub_unit_fake']
            a.address=values['address']
            a.latitude=values['latitude']
            a.longitude=values['longitude']
            a.available_as=values['available_as']
            a.number_of_guests=values['number_of_guests']
            a.number_of_rooms=values['number_of_rooms']
            a.number_of_beds=values['number_of_beds']
            a.number_of_baths=values['number_of_baths']
            
            a.save()
            print("here")
            return JsonResponse({"success": "Unit updated successfully"}, status=200)
        except:
            return JsonResponse({"error":"Error in Unit update" }, status=400)
    return JsonResponse({"error":"Error in Unit update" }, status=400)        